#kalkulator bmi
def bmi(masa, wzrost):
    return masa / (wzrost ** 2)
def sprawdzenie(bmi):
    if bmi < 18.5:
        print ("Twoje BMI wynosi ", bmi, ", masz niedowagę")
    elif bmi > 18.5 and bmi < 24.99:
        print ("Twoje BMI wynosi ", bmi, ", waga jest prawidłowa")
    else:
        print ("Twoje BMI wynosi ", bmi, ", masz nadwagę")
#main
print ("Program policzy Twoje BMI na podstawie wagi wpisanej w kg i wzrostu wpisanego w cm. ")
m = float(input("Podaj swoją wagę [kg]: "))
w = float(input("Podaj swój wzrost [cm]: "))
w = w / 100
bmi = bmi(m,w)
sprawdzenie(bmi)
input("Zakończ program naciskając ENTER")
