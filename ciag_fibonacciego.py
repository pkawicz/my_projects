#Ciąg Fibonacciego
#program ma za zadanie wypisać n wyrazów ciągu fibonacciego
#n podane przez użytkownika

#start

print("Program wypisze Ciąg Fibonacciego o ilości podanej przez Ciebie (n)")
n = int(input("Podaj n: "))
first = 0
second = 1
for i in range (n - 1):
    element = first + second
    print(element, end = " ")
    first = second
    second = element
print("\n")
input ("Aby zakończyć program, naciśnij Enter...")
