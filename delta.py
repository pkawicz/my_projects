#program do liczenia delty
#wprowadzenie
#użytkownik wprowadza 3 zmienne a, b i c.
#na ich podstawie program liczy deltę ze wzoru b^2 - 4ac, jeżeli delta>0
#równanie ma dwa rozwiązania, jeżeli delta = 0, ma jedno, jeżeli delta <0
#brak rozwiązania. Jeżeli a == 0 przerywa program (program liczy pierwiastki
#funckcji kwadratowej gdy a == 0, funkcja nie jest kwadratowa, a liniowa)
#START
from math import sqrt #pobranie funkcji pierwiastkowania z moudłu math
#ekran startowy
print ("\t\t\tPodstawowe Ćwiczenie Programisty")
print ("\t\t\tProgram liczący deltę")
#podanie zmiennych
a = int(input("Podaj zmienną a: "))
b = int(input("Podaj zmienną b: "))
c = int(input("Podaj zmienną c: "))
print(a,"x^2 + ", b, "x + ", c)
#obliczanie delty
delta = (b**b) - (4 * a * c)
if a == 0:
    print ("Równanie nie jest kwadratowe! Program zostanie zakończony.")
elif delta < 0:
    print("Równanie nie ma rozwiązania!")
elif delta == 0:
    x0 = (-b) /(2 * a)
    print("Równanie ma jedno rozwiązanie, jest nim x = ", x0)
else:
    x1 = ((-b)-(sqrt(delta))) / (2 * a)
    x2 = ((-b)+(sqrt(delta))) / (2 * a)
    print("Równanie ma dwa rozwiązania, są nimi x1 = ", x1, " i x2 = ", x2)
input("Naciśnij ENTER aby zakończyć program")
               
