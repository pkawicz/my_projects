#konwersja liczby dziesiętnej na liczbę binarną
#start
#funkcja przeliczająca
def convToBin(number):
    binar = []
    while number >= 1:
        binar.append(number % 2)
        number = number // 2
    i = len(binar) - 1
    while i >= 0:
        print (binar[i], end = "")
        i-=1
#main
#podanie numeru do konwersji
numer = int(input("Podaj liczbę całkowitą którą chcesz przekonwertować na postać binarną: "))
#wywołanie funkcji
convToBin(numer)
print ("\n")
input("Koniec.")
