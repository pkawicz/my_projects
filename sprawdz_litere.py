#sprawdzanie która litera alfabetu
alfabet = ["a", "ą", "b", "c", "ć", "d", "e", "ę", "f", "g", "h", "i", "j", "k", "l", "ł", "m", "n", "ń", "o", "ó", "p", "r", "s", "ś", "t", "u", "w", "y", "z", "ź", "ż"]
#powyżej alfabet, poniżej funkcja sprawdzająca
def sprawdz(litera):
    if litera not in alfabet:
        print("Litera nie należy do polskiego alfabetu")
    else:
        for x in alfabet:
            if litera == x:
                return alfabet.index(litera) + 1
                break
#main
literka = input("Podaj literę z polskiego alfabetu: ")
sprawdzenie = sprawdz(literka.lower())
if sprawdzenie:
    print (literka, " jest ", sprawdzenie, " elementem alfabetu")
input("Zakończ program naciskając ENTER")
        
